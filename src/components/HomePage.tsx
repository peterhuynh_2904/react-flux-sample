import React from "react";
import { Link } from "react-router-dom";

export const HomePage = () => {
  return (
    <div className="jumbotron">
      <h1>Administration</h1>
      <p>React, Flux, and React Router.</p>
      <Link to="about" className="btn btn-primary">
        About
      </Link>
    </div>
  );
};
