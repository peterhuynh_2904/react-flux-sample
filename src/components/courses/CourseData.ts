export interface ICourse {
    id: number;
    slug: string;
    title: String;
    category: String;
    authorId: Number;
}
export interface ICourseList {
    courses: ICourse[];
}