import React from "react";

import { ICourseList, ICourse } from "./CourseData";
import { Link } from "react-router-dom";

export const Courselist = (props: ICourseList) => {
  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th>Title</th>
            <th>Author Id</th>
            <th>Category</th>
          </tr>
        </thead>
        <tbody>
          {props.courses.map((course: ICourse) => {
            return (
              <tr key={course.id}>
                <td>
                  <Link to={`/course/${course.slug}`}>{course.title}</Link>
                </td>
                <td>{course.authorId}</td>
                <td>{course.category}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};
