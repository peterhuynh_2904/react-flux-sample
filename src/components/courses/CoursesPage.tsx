import React, { useState, useEffect } from "react";
import { getCourses } from "api/courseApi";
import { Courselist } from "./CourseList";
import { Link } from "react-router-dom";

export const CoursesPage = () => {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    getCourses().then((_courses) => setCourses(_courses));
  }, []);

  return (
    <>
      <h2>Courses Page</h2>
      <div>
        <Link className="btn btn-primary" to="/course">
          Add Course
        </Link>
      </div>
      <Courselist courses={courses} />
    </>
  );
};
