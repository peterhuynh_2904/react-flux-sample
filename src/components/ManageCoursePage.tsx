import React, { useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import { CourseForm } from "./courses/CourseForm";

export const ManageCoursePage = (
  props: RouteComponentProps<{ slug: string }>
) => {
  const [course, setCourse] = useState({
    id: null,
    slug: "",
    title: "",
    authorId: null,
    category: "",
  });

  const handleChange = ({ target }: any) => {
    const updatedCourse = {
      ...course,
      [target.name]: target.value,
    };
    setCourse(updatedCourse);
  };

  return (
    <>
      <h2>Manage Course</h2>
      <CourseForm course={course} onChange={handleChange} />
    </>
  );
};
