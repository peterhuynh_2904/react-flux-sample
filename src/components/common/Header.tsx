import React from "react";
import { NavLink } from "react-router-dom";

export interface IRoute {
  name: string;
  url: string;
  isExact?: boolean;
}
export const Header = () => {
  const activeStyle = { color: "orange" };
  const routes: IRoute[] = [
    { name: "Home", url: "/", isExact: true },
    { name: "About", url: "/about", isExact: false },
    { name: "Courses", url: "/courses", isExact: false },
  ];
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          {routes.map((route: IRoute) => {
            return (
              <li className="nav-item" key={route.name}>
                <NavLink
                  to={route.url}
                  activeStyle={activeStyle}
                  exact={route.isExact}
                  className="nav-link"
                >
                  {route.name}
                </NavLink>
              </li>
            );
          })}
        </ul>
      </div>
    </nav>
  );
};
